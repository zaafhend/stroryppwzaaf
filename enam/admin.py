from django.contrib import admin
from .models import KegiatanModel, PesertaModel

admin.site.register(KegiatanModel)
admin.site.register(PesertaModel)
