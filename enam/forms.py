from django.forms import ModelForm
from .models import KegiatanModel, PesertaModel

class BaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class Kegiatan_Form(BaseForm):
    class Meta:
        model = KegiatanModel
        fields = ['nama']
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama Kegiatan'
    }

class Peserta_Form(BaseForm):
	class Meta:
		model = PesertaModel
		fields = ['nama', 'kegiatan']
	error_messages = {
		'required' : 'Please Type'
	}
	input_attrs = {
		'type' : 'text'
	}

