from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *

class EnamTest(TestCase):

    def test_kegiatan_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_url_function(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, kegiatan)

    def test_template_used(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'base.html')

    def test_create_kegiatan(self):
        kegiatan_baru = KegiatanModel.objects.create(nama='Tidur')
        berapa_kegiatan = KegiatanModel.objects.all().count()
        self.assertEqual(berapa_kegiatan, 1)

    def test_create_peserta(self):
        kegiatan_baru = KegiatanModel.objects.create(nama='Tidur')
        peserta_baru = PesertaModel.objects.create(nama = 'Budi', kegiatan = KegiatanModel.objects.get(nama='Tidur'))
        berapa_peserta = PesertaModel.objects.all().count()
        self.assertEqual(berapa_peserta, 1)

    def test_page_include_new_models(self):
        kegiatan_baru = KegiatanModel.objects.create(nama='Tidur')
        peserta_baru = PesertaModel.objects.create(nama = 'Budi', kegiatan = KegiatanModel.objects.get(nama='Tidur'))

        new_response = self.client.get('/kegiatan/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Budi', html_response)