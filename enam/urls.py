from django.urls import path

from . import views

app_name = 'enam'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('savePeserta/', views.savePeserta, name='peserta'),
]
