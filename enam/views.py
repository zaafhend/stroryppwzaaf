from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.shortcuts import redirect
from .forms import Kegiatan_Form, Peserta_Form
from .models import KegiatanModel, PesertaModel

def kegiatan(request):
    form_kegiatan = Kegiatan_Form(request.POST or None)
    form_peserta = Peserta_Form(request.POST or None)

    if (form_kegiatan.is_valid() and  request.method == 'POST' and 'kegiatan' in request.POST):
        form_kegiatan.save()

    variables = {'form_kegiatan': form_kegiatan,
            'form_peserta': form_peserta,
            'kegiatan': KegiatanModel.objects.all(),
            'peserta': PesertaModel.objects.all()}

    return render(request, 'enam/kegiatan.html', variables)

def savePeserta(request):
    form_peserta = Peserta_Form(request.POST or None)
    if (form_peserta.is_valid() and request.method == 'POST' and 'peserta' in request.POST):
        form_peserta.save()

    return redirect('/kegiatan')