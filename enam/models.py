from django.db import models

class KegiatanModel(models.Model):
    nama = models.CharField(max_length=40)

    def __str__(self):
       return self.nama

class PesertaModel(models.Model):
    nama = models.CharField(max_length=50)
    kegiatan = models.ForeignKey(KegiatanModel, on_delete=models.CASCADE)



