from django.urls import path

from . import views

app_name = 'tujuh'

urlpatterns = [
    path('', views.accordion, name='accordion'),
]
