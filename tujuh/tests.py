from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class EnamTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_url_function(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, accordion)

    def test_template_used(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'tujuh/accor.html')
