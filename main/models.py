from django.db import models

class Matkul(models.Model):
    nama = models.TextField()
    dosen = models.TextField()
    deskripsi = models.TextField()
    tahunajar = models.TextField()
    sks = models.IntegerField()
    kelas = models.TextField()

