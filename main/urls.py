from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('education', views.education, name='education'),
    path('matkul', views.matkul, name='matkul'),
    path('matkul/<int:id>', 
                       views.matkulDetail,
                       name='detail'),
    path('matkul/<int:id>/delete', 
                       views.hapus,
                       name='hapus'),
]
