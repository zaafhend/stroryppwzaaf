from django.shortcuts import render
from django.shortcuts import redirect
from .models import Matkul

def home(request):
    return render(request, 'main/home.html')

def education(request):
    return render(request, 'main/education.html')

def matkul(request):
    if request.method == 'POST':
        matkul = Matkul() 
        matkul.nama = request.POST['nama']
        matkul.dosen = request.POST['dosen']
        matkul.deskripsi = request.POST['deskripsi']
        matkul.tahunajar = request.POST['tahunajar']
        matkul.sks = request.POST['sks']
        matkul.kelas = request.POST['kelas']
        matkul.save()
    
    variables = {
        "matkulnya": Matkul.objects.all(),
    }

    return render(request, 'main/matkul.html', variables)

def matkulDetail(request, id):
    matkul = Matkul.objects.get(id=id)
    variables = {
        "matkul" : matkul
    }
    return render(request, 'main/matkulDetails.html', variables)

def hapus(request, id):
    matkul = Matkul.objects.get(id=id)
    matkul.delete()
    return redirect('/matkul')