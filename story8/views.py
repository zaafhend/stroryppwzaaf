from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib import messages
from .forms import UserRegister

def search(request):
    return render(request, 'story8/search.html', {})

def result(request):
    keyword = request.GET['q']
    url_result = 'https://www.googleapis.com/books/v1/volumes?q=' + keyword
    r = requests.get(url_result)

    data = json.loads(r.content)
    return JsonResponse(data, safe=False)

def register(request):
    if request.method == 'POST':
        form = UserRegister(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Account Succesfully Registered')
            return redirect('/search/login/')
    else:    
        form = UserRegister()
    return render(request, 'story8/register.html', {'form': form})
