from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import *
from .apps import *
from .forms import *

class SearchTest(TestCase):

    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')

    def test_url_is_exist(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_url_function(self):
        found = resolve('/search/')
        self.assertEqual(found.func, search)

    def test_template_used(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response, 'story8/search.html')
        

    def test_json_returned(self):
        new_response = self.client.get('/search/result?q=management')
        html_response = new_response.content.decode('utf8')
        self.assertIn('management', html_response)


class LoginTest(TestCase):
    def test_url_register(self):
        responses = Client().get("/search/newuser/")
        self.assertEquals(responses.status_code, 200)
    
    def test_template_register(self):
        responses = Client().get("/search/newuser/")
        self.assertTemplateUsed(responses, 'story8/register.html')
    
    def test_create_user(self):
        user = {
            'username' : 'ppw',
            'email' : 'kakpewe@ppw.com',
            'password1' : 'akuuserbaru',
            'password2' : 'akuuserbaru',
        }

        responses = self.client.post('/search/newuser/', user)
        testuser = User.objects.get(email = 'kakpewe@ppw.com')
        self.assertEqual(testuser.username, 'ppw')

    def test_url_login(self):
        responses = Client().get("/search/login/")
        self.assertEquals(responses.status_code, 200)
    
    def test_template_login(self):
        responses = Client().get("/search/login/")
        self.assertTemplateUsed(responses, 'story8/login.html')

    def test_url_logout(self):
        responses = Client().get("/search/logout/")
        self.assertEquals(responses.status_code, 302)


    
