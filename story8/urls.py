from django.urls import path
from django.conf.urls import url
from django.contrib.auth import views as authviews
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.search, name='search'),
    path('result', views.result, name='result'),
    path('newuser/', views.register, name='register'),
    path('login/', authviews.LoginView.as_view(template_name='story8/login.html'), name='login'),
    path('logout/', authviews.LogoutView.as_view(template_name='story8/search.html'), name='logout'),
]